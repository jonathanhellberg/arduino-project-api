import asyncio
import serial
import time
from flask import Flask, request
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

arduino = serial.Serial('/dev/ttyACM1', 9600)

loop = asyncio.get_event_loop()

@app.route('/api/send/', methods=['POST', 'OPTIONS'])
@cross_origin()
def SendData():
        print("Connection Created")
        data = loop.run_until_complete(write_read(request.form['id']))
        print("Lamp scheduled")
        return data
        

@app.route('/api/temp/', methods=['GET', 'OPTIONS'])
@cross_origin()
def GetTemp():
        data = write_read("temperature")
        return data

@app.route('/api/humi/', methods=['GET', 'OPTIONS'])
@cross_origin()
def GetHumi():
        data = write_read("humidity")
        return data

async def write_read(x):
        await asyncio.sleep(0.5)
        arduino.write(x.encode())

        line = arduino.readline().decode('utf-8').rstrip()
        print(line)
        return line


app.run(host='0.0.0.0')